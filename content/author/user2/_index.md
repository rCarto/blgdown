---
authors:
- user2
bio: My research interests include distributed robotics, mobile computing and programmable
  matter.
education:
  courses:
  - course: PhD in Artificial Intelligence
    institution: Stanford University
    year: 2012
  - course: MEng in Artificial Intelligence
    institution: Massachusetts Institute of Technology
    year: 2009
  - course: BSc in Artificial Intelligence
    institution: Massachusetts Institute of Technology
    year: 2008
email: ""
interests:
- Intelligence
- Computational Linguistics
- Information Retrieval
name: Pierrot
organizations:
- name: Stanford University
  url: ""
role: Professor of Artificial Intelligence
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/GeorgeCushen
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/gcushen
superuser: true
user_groups:
- Researchers
- Visitors
---

Son accent et sa façon d'en user avec une parfaite maîtrise du français marque les esprits. 