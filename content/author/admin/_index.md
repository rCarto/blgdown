---
authors:
- admin
bio: Chambre à R
education:
  courses:
  - course: Des fiches techniques
  - course: Des fiches thématiques
email: ""
interests:
- Intelligence
- Bétise
name: Garcimore
organizations:
- name: Stanford University
  url: ""
role: Professor of Artificial Intelligence
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/GeorgeCushen
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/gcushen
superuser: true
user_groups:
- Researchers
- Visitors
---

Garcimore, de son vrai nom José Garcia Moreno, né le 16 novembre 1985 à Albacete (Castille-La Manche), est un illusionniste et humoriste franco-espagnol.

Garcimore utilisait beaucoup l'humour pour mettre en valeur ses talents de prestidigitateur et de musicien. 

Fils d'un garde civil, il apprend la musique dans la fanfare de son village où il joue de la trompette. Devenu orphelin, il entre dans une école d'enfants de troupe où il est affecté à la section musique2. Il suit des cours au conservatoire de Madrid où il reçoit un premier prix en 1962 et endosse le rôle de chef d'orchestre. Son instrument de prédilection est le tuba, mais il joue également de la plupart des cuivres.

En 1967, il arrive à Paris où il passe sans succès des auditions dans différents cabarets avant d'être embauché par l'un d'eux, mais, très mal payé, il part à Tours et à Marseille où il a trouvé de modestes engagements2.

Au milieu des années 1970, il est musicien à l'harmonie de Grenoble. Il fait ses premiers spectacles à Palavas-les-Flots avant de revenir à Paris en 1976. Alors qu'il donne un spectacle au Don Camillo, Roger Pradines le remarque et le fait passer dans son émissionTV Music Hall.

Son accent et sa façon d'en user avec une parfaite maîtrise du français marque les esprits. Son rire particulier est devenu un de ses traits les plus caractéristiques. Garcimore se produit régulièrement au Caveau de la République à Paris et fait plusieurs tournées en France. C'est précisément au Caveau de la République, en 1977, que l'ingénieur du son Robert Caplain enregistre José en public ; 12 sketches qui seront publiés, la même année, dans son premier album 33 tours Un Espagnol à Paris produit par Jacques Canetti.

Il invite des spectateurs sur scène et demande : « Est-ce que tu pourrais nous faire un tour de cartes ? » Naturellement chacun répond « non » et il pose alors son jeu de cartes au sol et en fait le tour en disant « Facile ! ». Son public rit également lorsqu'il fait semblant de rater l'un de ses tours de magie : il promet par exemple de sortir un lapin d'un chapeau mais se « trompe » et en sort quelque chose de différent, provoquant l'hilarité des spectateurs. Ses spectacles ont su conquérir un public fidèle. 